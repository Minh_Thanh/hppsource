//Revision: 04341d2a-57a9-41e6-8624-0737a6c36ea2






























namespace HPPSource.Models.sitecore.templates.System.Analytics
{


 	/// <summary>
	/// ICampaign Interface
	/// <para></para>
	/// <para>Path: /sitecore/templates/System/Analytics/Campaign</para>	
	/// <para>ID: 94fd1606-139e-46ee-86ff-bc5bf3c79804</para>	
	/// </summary>
	[SitecoreType(TemplateId=ICampaignConstants.TemplateIdString )] //, Cachable = true
	public partial interface ICampaign : IGlassBase 
	{
			
	}


	public static partial class ICampaignConstants{

			public const string TemplateIdString = "94fd1606-139e-46ee-86ff-bc5bf3c79804";
			public static readonly ID TemplateId = new ID(TemplateIdString);
			public const string TemplateName = "Campaign";

			

	}

	
	/// <summary>
	/// Campaign
	/// <para></para>
	/// <para>Path: /sitecore/templates/System/Analytics/Campaign</para>	
	/// <para>ID: 94fd1606-139e-46ee-86ff-bc5bf3c79804</para>	
	/// </summary>
	[SitecoreType(TemplateId=ICampaignConstants.TemplateIdString)] //, Cachable = true
	public partial class Campaign  : GlassBase, ICampaign 
	{
	   
			
	}

}

