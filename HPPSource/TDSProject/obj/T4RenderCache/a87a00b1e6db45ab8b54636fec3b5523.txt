//Revision: fefa69bc-0cf1-4e34-b310-8bbfc1f5003d






























namespace HPPSource.Models.sitecore.templates.Common
{


 	/// <summary>
	/// IFolder Interface
	/// <para></para>
	/// <para>Path: /sitecore/templates/Common/Folder</para>	
	/// <para>ID: a87a00b1-e6db-45ab-8b54-636fec3b5523</para>	
	/// </summary>
	[SitecoreType(TemplateId=IFolderConstants.TemplateIdString )] //, Cachable = true
	public partial interface IFolder : IGlassBase 
	{
			
	}


	public static partial class IFolderConstants{

			public const string TemplateIdString = "a87a00b1-e6db-45ab-8b54-636fec3b5523";
			public static readonly ID TemplateId = new ID(TemplateIdString);
			public const string TemplateName = "Folder";

			

	}

	
	/// <summary>
	/// Folder
	/// <para></para>
	/// <para>Path: /sitecore/templates/Common/Folder</para>	
	/// <para>ID: a87a00b1-e6db-45ab-8b54-636fec3b5523</para>	
	/// </summary>
	[SitecoreType(TemplateId=IFolderConstants.TemplateIdString)] //, Cachable = true
	public partial class Folder  : GlassBase, IFolder 
	{
	   
			
	}

}

