//Revision: eb7a9e29-2ab6-462e-b220-b4a9f2bc8f4e






























namespace HPPSource.Models.sitecore.templates.System
{


 	/// <summary>
	/// IReference Interface
	/// <para></para>
	/// <para>Path: /sitecore/templates/System/Reference</para>	
	/// <para>ID: ef295cd8-19d4-4e02-9438-94c926ef5284</para>	
	/// </summary>
	[SitecoreType(TemplateId=IReferenceConstants.TemplateIdString )] //, Cachable = true
	public partial interface IReference : IGlassBase 
	{
			
	}


	public static partial class IReferenceConstants{

			public const string TemplateIdString = "ef295cd8-19d4-4e02-9438-94c926ef5284";
			public static readonly ID TemplateId = new ID(TemplateIdString);
			public const string TemplateName = "Reference";

			

	}

	
	/// <summary>
	/// Reference
	/// <para></para>
	/// <para>Path: /sitecore/templates/System/Reference</para>	
	/// <para>ID: ef295cd8-19d4-4e02-9438-94c926ef5284</para>	
	/// </summary>
	[SitecoreType(TemplateId=IReferenceConstants.TemplateIdString)] //, Cachable = true
	public partial class Reference  : GlassBase, IReference 
	{
	   
			
	}

}

