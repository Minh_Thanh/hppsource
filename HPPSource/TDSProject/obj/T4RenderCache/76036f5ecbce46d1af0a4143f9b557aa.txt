//Revision: 46b9028c-b428-4ede-a25e-dc1dbe416bb7






























namespace HPPSource.Models.sitecore.templates.Sample
{


 	/// <summary>
	/// ISample_Item Interface
	/// <para></para>
	/// <para>Path: /sitecore/templates/Sample/Sample Item</para>	
	/// <para>ID: 76036f5e-cbce-46d1-af0a-4143f9b557aa</para>	
	/// </summary>
	[SitecoreType(TemplateId=ISample_ItemConstants.TemplateIdString )] //, Cachable = true
	public partial interface ISample_Item : IGlassBase 
	{
			
	}


	public static partial class ISample_ItemConstants{

			public const string TemplateIdString = "76036f5e-cbce-46d1-af0a-4143f9b557aa";
			public static readonly ID TemplateId = new ID(TemplateIdString);
			public const string TemplateName = "Sample Item";

			

	}

	
	/// <summary>
	/// Sample_Item
	/// <para></para>
	/// <para>Path: /sitecore/templates/Sample/Sample Item</para>	
	/// <para>ID: 76036f5e-cbce-46d1-af0a-4143f9b557aa</para>	
	/// </summary>
	[SitecoreType(TemplateId=ISample_ItemConstants.TemplateIdString)] //, Cachable = true
	public partial class Sample_Item  : GlassBase, ISample_Item 
	{
	   
			
	}

}

