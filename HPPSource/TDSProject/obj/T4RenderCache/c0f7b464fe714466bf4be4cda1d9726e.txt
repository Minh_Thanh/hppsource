//Revision: 9197850a-061a-4065-b213-b3a2e48cb91a






























namespace HPPSource.Models.sitecore.templates.HPPSource.Project.Page_Content.Impact
{


 	/// <summary>
	/// IContent_Right Interface
	/// <para></para>
	/// <para>Path: /sitecore/templates/HPPSource/Project/Page Content/Impact/Content Right</para>	
	/// <para>ID: c0f7b464-fe71-4466-bf4b-e4cda1d9726e</para>	
	/// </summary>
	[SitecoreType(TemplateId=IContent_RightConstants.TemplateIdString )] //, Cachable = true
	public partial interface IContent_Right : IGlassBase 
	{
			
					/// <summary>
					/// The Description field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: 31d8bc4f-dc80-4c99-a62c-2b63a9b1f72e</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_RightConstants.DescriptionFieldName)]
					string Description  {get; set;}
			
			
					/// <summary>
					/// The Title1 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: ed20cd63-734d-4922-9401-daeaf49e024b</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_RightConstants.Title1FieldName)]
					string Title1  {get; set;}
			
			
					/// <summary>
					/// The Title2 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: 37ee5668-f8ac-4a9a-9e7b-26542f27b427</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_RightConstants.Title2FieldName)]
					string Title2  {get; set;}
			
			
					/// <summary>
					/// The URL Target1 field.
					/// <para></para>
					/// <para>Field Type: General Link</para>		
					/// <para>Field ID: fddec00e-8bed-4b6f-831f-44b87f9acedf</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_RightConstants.URL_Target1FieldName)]
					Link URL_Target1  {get; set;}
			
			
					/// <summary>
					/// The URL Target2 field.
					/// <para></para>
					/// <para>Field Type: General Link</para>		
					/// <para>Field ID: 5cb7eee1-80c7-4432-a8f1-abe8693efbd1</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_RightConstants.URL_Target2FieldName)]
					Link URL_Target2  {get; set;}
			
			
	}


	public static partial class IContent_RightConstants{

			public const string TemplateIdString = "c0f7b464-fe71-4466-bf4b-e4cda1d9726e";
			public static readonly ID TemplateId = new ID(TemplateIdString);
			public const string TemplateName = "Content Right";

		
			
			public static readonly ID DescriptionFieldId = new ID("31d8bc4f-dc80-4c99-a62c-2b63a9b1f72e");
			public const string DescriptionFieldName = "Description";
			
		
			
			public static readonly ID Title1FieldId = new ID("ed20cd63-734d-4922-9401-daeaf49e024b");
			public const string Title1FieldName = "Title1";
			
		
			
			public static readonly ID Title2FieldId = new ID("37ee5668-f8ac-4a9a-9e7b-26542f27b427");
			public const string Title2FieldName = "Title2";
			
		
			
			public static readonly ID URL_Target1FieldId = new ID("fddec00e-8bed-4b6f-831f-44b87f9acedf");
			public const string URL_Target1FieldName = "URL Target1";
			
		
			
			public static readonly ID URL_Target2FieldId = new ID("5cb7eee1-80c7-4432-a8f1-abe8693efbd1");
			public const string URL_Target2FieldName = "URL Target2";
			
			

	}

	
	/// <summary>
	/// Content_Right
	/// <para></para>
	/// <para>Path: /sitecore/templates/HPPSource/Project/Page Content/Impact/Content Right</para>	
	/// <para>ID: c0f7b464-fe71-4466-bf4b-e4cda1d9726e</para>	
	/// </summary>
	[SitecoreType(TemplateId=IContent_RightConstants.TemplateIdString)] //, Cachable = true
	public partial class Content_Right  : GlassBase, IContent_Right 
	{
	   
		
				/// <summary>
				/// The Description field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: 31d8bc4f-dc80-4c99-a62c-2b63a9b1f72e</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_RightConstants.DescriptionFieldName)]
				public virtual string Description  {get; set;}
					
		
				/// <summary>
				/// The Title1 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: ed20cd63-734d-4922-9401-daeaf49e024b</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_RightConstants.Title1FieldName)]
				public virtual string Title1  {get; set;}
					
		
				/// <summary>
				/// The Title2 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: 37ee5668-f8ac-4a9a-9e7b-26542f27b427</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_RightConstants.Title2FieldName)]
				public virtual string Title2  {get; set;}
					
		
				/// <summary>
				/// The URL Target1 field.
				/// <para></para>
				/// <para>Field Type: General Link</para>		
				/// <para>Field ID: fddec00e-8bed-4b6f-831f-44b87f9acedf</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_RightConstants.URL_Target1FieldName)]
				public virtual Link URL_Target1  {get; set;}
					
		
				/// <summary>
				/// The URL Target2 field.
				/// <para></para>
				/// <para>Field Type: General Link</para>		
				/// <para>Field ID: 5cb7eee1-80c7-4432-a8f1-abe8693efbd1</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_RightConstants.URL_Target2FieldName)]
				public virtual Link URL_Target2  {get; set;}
					
			
	}

}

