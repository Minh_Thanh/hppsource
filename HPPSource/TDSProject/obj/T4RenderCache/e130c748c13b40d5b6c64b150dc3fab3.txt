//Revision: 6cfcd1e9-7211-435c-8898-10c2a96d3039






























namespace HPPSource.Models.sitecore.templates.System
{


 	/// <summary>
	/// IPublishing_Target Interface
	/// <para></para>
	/// <para>Path: /sitecore/templates/System/Publishing target</para>	
	/// <para>ID: e130c748-c13b-40d5-b6c6-4b150dc3fab3</para>	
	/// </summary>
	[SitecoreType(TemplateId=IPublishing_TargetConstants.TemplateIdString )] //, Cachable = true
	public partial interface IPublishing_Target : IGlassBase 
	{
			
	}


	public static partial class IPublishing_TargetConstants{

			public const string TemplateIdString = "e130c748-c13b-40d5-b6c6-4b150dc3fab3";
			public static readonly ID TemplateId = new ID(TemplateIdString);
			public const string TemplateName = "Publishing target";

			

	}

	
	/// <summary>
	/// Publishing_Target
	/// <para></para>
	/// <para>Path: /sitecore/templates/System/Publishing target</para>	
	/// <para>ID: e130c748-c13b-40d5-b6c6-4b150dc3fab3</para>	
	/// </summary>
	[SitecoreType(TemplateId=IPublishing_TargetConstants.TemplateIdString)] //, Cachable = true
	public partial class Publishing_Target  : GlassBase, IPublishing_Target 
	{
	   
			
	}

}

