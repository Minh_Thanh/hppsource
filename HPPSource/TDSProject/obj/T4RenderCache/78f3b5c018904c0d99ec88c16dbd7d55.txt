//Revision: b6048e30-5836-4e69-a058-632162f83484






























namespace HPPSource.Models.sitecore.templates.HPPSource.Project.Content_Type
{


 	/// <summary>
	/// IDatasources Interface
	/// <para></para>
	/// <para>Path: /sitecore/templates/HPPSource/Project/Content Type/Datasources</para>	
	/// <para>ID: 78f3b5c0-1890-4c0d-99ec-88c16dbd7d55</para>	
	/// </summary>
	[SitecoreType(TemplateId=IDatasourcesConstants.TemplateIdString )] //, Cachable = true
	public partial interface IDatasources : IGlassBase 
	{
			
	}


	public static partial class IDatasourcesConstants{

			public const string TemplateIdString = "78f3b5c0-1890-4c0d-99ec-88c16dbd7d55";
			public static readonly ID TemplateId = new ID(TemplateIdString);
			public const string TemplateName = "Datasources";

			

	}

	
	/// <summary>
	/// Datasources
	/// <para></para>
	/// <para>Path: /sitecore/templates/HPPSource/Project/Content Type/Datasources</para>	
	/// <para>ID: 78f3b5c0-1890-4c0d-99ec-88c16dbd7d55</para>	
	/// </summary>
	[SitecoreType(TemplateId=IDatasourcesConstants.TemplateIdString)] //, Cachable = true
	public partial class Datasources  : GlassBase, IDatasources 
	{
	   
			
	}

}

