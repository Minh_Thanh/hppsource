//Revision: ed6815ca-d0b0-4809-bfe4-5981d1f41ce4






























namespace HPPSource.Models.sitecore.templates.HPPSource.Project.Page_Content.Impact
{


 	/// <summary>
	/// IContent_Para Interface
	/// <para></para>
	/// <para>Path: /sitecore/templates/HPPSource/Project/Page Content/Impact/Content Para</para>	
	/// <para>ID: 947bc50d-8a14-4df2-a835-44f42c1ef971</para>	
	/// </summary>
	[SitecoreType(TemplateId=IContent_ParaConstants.TemplateIdString )] //, Cachable = true
	public partial interface IContent_Para : IGlassBase 
	{
			
					/// <summary>
					/// The Description1 field.
					/// <para></para>
					/// <para>Field Type: Multi-Line Text</para>		
					/// <para>Field ID: 3bf54904-d6c6-4c9e-8f15-8dac2b343bbe</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Description1FieldName)]
					string Description1  {get; set;}
			
			
					/// <summary>
					/// The Description2 field.
					/// <para></para>
					/// <para>Field Type: Multi-Line Text</para>		
					/// <para>Field ID: 1b22d153-2b04-4b38-8600-361923e3c80b</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Description2FieldName)]
					string Description2  {get; set;}
			
			
					/// <summary>
					/// The Description3 field.
					/// <para></para>
					/// <para>Field Type: Multi-Line Text</para>		
					/// <para>Field ID: 45896709-c788-4cb3-926d-83363cfad2e6</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Description3FieldName)]
					string Description3  {get; set;}
			
			
					/// <summary>
					/// The Description4 field.
					/// <para></para>
					/// <para>Field Type: Multi-Line Text</para>		
					/// <para>Field ID: 5d18d756-20f4-46f4-8be9-de55cf23a8e9</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Description4FieldName)]
					string Description4  {get; set;}
			
			
					/// <summary>
					/// The Img Arrow Hover field.
					/// <para></para>
					/// <para>Field Type: Image</para>		
					/// <para>Field ID: 4312a68a-a6db-4f13-910f-e46327791f73</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Img_Arrow_HoverFieldName)]
					Image Img_Arrow_Hover  {get; set;}
			
			
					/// <summary>
					/// The Img Arrow field.
					/// <para></para>
					/// <para>Field Type: Image</para>		
					/// <para>Field ID: 62ec4b47-e1b1-4f7c-8466-b8f941fdc263</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Img_ArrowFieldName)]
					Image Img_Arrow  {get; set;}
			
			
					/// <summary>
					/// The Img field.
					/// <para></para>
					/// <para>Field Type: Image</para>		
					/// <para>Field ID: d51e4bdf-f974-47a2-8894-ac6f1b83d888</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.ImgFieldName)]
					Image Img  {get; set;}
			
			
					/// <summary>
					/// The Li1 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: 00329213-1505-491f-b86f-598344160c80</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Li1FieldName)]
					string Li1  {get; set;}
			
			
					/// <summary>
					/// The Li2 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: 78292598-7d4c-44cf-b581-225f9a0dad66</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Li2FieldName)]
					string Li2  {get; set;}
			
			
					/// <summary>
					/// The References field.
					/// <para></para>
					/// <para>Field Type: Rich Text</para>		
					/// <para>Field ID: ffe033a2-7828-4381-9a5e-492ef78c1e55</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.ReferencesFieldName)]
					string References  {get; set;}
			
			
					/// <summary>
					/// The Sup Description1 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: 1b1f97e7-5f50-453b-b4aa-2fb922fd3924</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Sup_Description1FieldName)]
					string Sup_Description1  {get; set;}
			
			
					/// <summary>
					/// The Sup Description2 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: aa1a537a-7ffb-4e1c-ba9c-1f34711fda7c</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Sup_Description2FieldName)]
					string Sup_Description2  {get; set;}
			
			
					/// <summary>
					/// The Sup Title2 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: d1637ce0-9cf0-419e-8d2d-d885ab61c9eb</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Sup_Title2FieldName)]
					string Sup_Title2  {get; set;}
			
			
					/// <summary>
					/// The Text field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: 3cfdefc7-30e2-4f14-900d-1da99c2f48c7</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.TextFieldName)]
					string Text  {get; set;}
			
			
					/// <summary>
					/// The Title1 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: e2730442-b25c-4b02-a959-89111a99d03f</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Title1FieldName)]
					string Title1  {get; set;}
			
			
					/// <summary>
					/// The Title2 field.
					/// <para></para>
					/// <para>Field Type: Single-Line Text</para>		
					/// <para>Field ID: 4385c3c2-6b6b-4754-bc65-bb5cdf510d54</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.Title2FieldName)]
					string Title2  {get; set;}
			
			
					/// <summary>
					/// The URL field.
					/// <para></para>
					/// <para>Field Type: General Link</para>		
					/// <para>Field ID: 3bf80f5c-7abd-4f8f-bce8-8c7acb7cb1f2</para>
					/// <para>Custom Data: </para>
					/// </summary>
					[SitecoreField(IContent_ParaConstants.URLFieldName)]
					Link URL  {get; set;}
			
			
	}


	public static partial class IContent_ParaConstants{

			public const string TemplateIdString = "947bc50d-8a14-4df2-a835-44f42c1ef971";
			public static readonly ID TemplateId = new ID(TemplateIdString);
			public const string TemplateName = "Content Para";

		
			
			public static readonly ID Description1FieldId = new ID("3bf54904-d6c6-4c9e-8f15-8dac2b343bbe");
			public const string Description1FieldName = "Description1";
			
		
			
			public static readonly ID Description2FieldId = new ID("1b22d153-2b04-4b38-8600-361923e3c80b");
			public const string Description2FieldName = "Description2";
			
		
			
			public static readonly ID Description3FieldId = new ID("45896709-c788-4cb3-926d-83363cfad2e6");
			public const string Description3FieldName = "Description3";
			
		
			
			public static readonly ID Description4FieldId = new ID("5d18d756-20f4-46f4-8be9-de55cf23a8e9");
			public const string Description4FieldName = "Description4";
			
		
			
			public static readonly ID Img_Arrow_HoverFieldId = new ID("4312a68a-a6db-4f13-910f-e46327791f73");
			public const string Img_Arrow_HoverFieldName = "Img Arrow Hover";
			
		
			
			public static readonly ID Img_ArrowFieldId = new ID("62ec4b47-e1b1-4f7c-8466-b8f941fdc263");
			public const string Img_ArrowFieldName = "Img Arrow";
			
		
			
			public static readonly ID ImgFieldId = new ID("d51e4bdf-f974-47a2-8894-ac6f1b83d888");
			public const string ImgFieldName = "Img";
			
		
			
			public static readonly ID Li1FieldId = new ID("00329213-1505-491f-b86f-598344160c80");
			public const string Li1FieldName = "Li1";
			
		
			
			public static readonly ID Li2FieldId = new ID("78292598-7d4c-44cf-b581-225f9a0dad66");
			public const string Li2FieldName = "Li2";
			
		
			
			public static readonly ID ReferencesFieldId = new ID("ffe033a2-7828-4381-9a5e-492ef78c1e55");
			public const string ReferencesFieldName = "References";
			
		
			
			public static readonly ID Sup_Description1FieldId = new ID("1b1f97e7-5f50-453b-b4aa-2fb922fd3924");
			public const string Sup_Description1FieldName = "Sup Description1";
			
		
			
			public static readonly ID Sup_Description2FieldId = new ID("aa1a537a-7ffb-4e1c-ba9c-1f34711fda7c");
			public const string Sup_Description2FieldName = "Sup Description2";
			
		
			
			public static readonly ID Sup_Title2FieldId = new ID("d1637ce0-9cf0-419e-8d2d-d885ab61c9eb");
			public const string Sup_Title2FieldName = "Sup Title2";
			
		
			
			public static readonly ID TextFieldId = new ID("3cfdefc7-30e2-4f14-900d-1da99c2f48c7");
			public const string TextFieldName = "Text";
			
		
			
			public static readonly ID Title1FieldId = new ID("e2730442-b25c-4b02-a959-89111a99d03f");
			public const string Title1FieldName = "Title1";
			
		
			
			public static readonly ID Title2FieldId = new ID("4385c3c2-6b6b-4754-bc65-bb5cdf510d54");
			public const string Title2FieldName = "Title2";
			
		
			
			public static readonly ID URLFieldId = new ID("3bf80f5c-7abd-4f8f-bce8-8c7acb7cb1f2");
			public const string URLFieldName = "URL";
			
			

	}

	
	/// <summary>
	/// Content_Para
	/// <para></para>
	/// <para>Path: /sitecore/templates/HPPSource/Project/Page Content/Impact/Content Para</para>	
	/// <para>ID: 947bc50d-8a14-4df2-a835-44f42c1ef971</para>	
	/// </summary>
	[SitecoreType(TemplateId=IContent_ParaConstants.TemplateIdString)] //, Cachable = true
	public partial class Content_Para  : GlassBase, IContent_Para 
	{
	   
		
				/// <summary>
				/// The Description1 field.
				/// <para></para>
				/// <para>Field Type: Multi-Line Text</para>		
				/// <para>Field ID: 3bf54904-d6c6-4c9e-8f15-8dac2b343bbe</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Description1FieldName)]
				public virtual string Description1  {get; set;}
					
		
				/// <summary>
				/// The Description2 field.
				/// <para></para>
				/// <para>Field Type: Multi-Line Text</para>		
				/// <para>Field ID: 1b22d153-2b04-4b38-8600-361923e3c80b</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Description2FieldName)]
				public virtual string Description2  {get; set;}
					
		
				/// <summary>
				/// The Description3 field.
				/// <para></para>
				/// <para>Field Type: Multi-Line Text</para>		
				/// <para>Field ID: 45896709-c788-4cb3-926d-83363cfad2e6</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Description3FieldName)]
				public virtual string Description3  {get; set;}
					
		
				/// <summary>
				/// The Description4 field.
				/// <para></para>
				/// <para>Field Type: Multi-Line Text</para>		
				/// <para>Field ID: 5d18d756-20f4-46f4-8be9-de55cf23a8e9</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Description4FieldName)]
				public virtual string Description4  {get; set;}
					
		
				/// <summary>
				/// The Img Arrow Hover field.
				/// <para></para>
				/// <para>Field Type: Image</para>		
				/// <para>Field ID: 4312a68a-a6db-4f13-910f-e46327791f73</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Img_Arrow_HoverFieldName)]
				public virtual Image Img_Arrow_Hover  {get; set;}
					
		
				/// <summary>
				/// The Img Arrow field.
				/// <para></para>
				/// <para>Field Type: Image</para>		
				/// <para>Field ID: 62ec4b47-e1b1-4f7c-8466-b8f941fdc263</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Img_ArrowFieldName)]
				public virtual Image Img_Arrow  {get; set;}
					
		
				/// <summary>
				/// The Img field.
				/// <para></para>
				/// <para>Field Type: Image</para>		
				/// <para>Field ID: d51e4bdf-f974-47a2-8894-ac6f1b83d888</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.ImgFieldName)]
				public virtual Image Img  {get; set;}
					
		
				/// <summary>
				/// The Li1 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: 00329213-1505-491f-b86f-598344160c80</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Li1FieldName)]
				public virtual string Li1  {get; set;}
					
		
				/// <summary>
				/// The Li2 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: 78292598-7d4c-44cf-b581-225f9a0dad66</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Li2FieldName)]
				public virtual string Li2  {get; set;}
					
		
				/// <summary>
				/// The References field.
				/// <para></para>
				/// <para>Field Type: Rich Text</para>		
				/// <para>Field ID: ffe033a2-7828-4381-9a5e-492ef78c1e55</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.ReferencesFieldName)]
				public virtual string References  {get; set;}
					
		
				/// <summary>
				/// The Sup Description1 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: 1b1f97e7-5f50-453b-b4aa-2fb922fd3924</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Sup_Description1FieldName)]
				public virtual string Sup_Description1  {get; set;}
					
		
				/// <summary>
				/// The Sup Description2 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: aa1a537a-7ffb-4e1c-ba9c-1f34711fda7c</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Sup_Description2FieldName)]
				public virtual string Sup_Description2  {get; set;}
					
		
				/// <summary>
				/// The Sup Title2 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: d1637ce0-9cf0-419e-8d2d-d885ab61c9eb</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Sup_Title2FieldName)]
				public virtual string Sup_Title2  {get; set;}
					
		
				/// <summary>
				/// The Text field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: 3cfdefc7-30e2-4f14-900d-1da99c2f48c7</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.TextFieldName)]
				public virtual string Text  {get; set;}
					
		
				/// <summary>
				/// The Title1 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: e2730442-b25c-4b02-a959-89111a99d03f</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Title1FieldName)]
				public virtual string Title1  {get; set;}
					
		
				/// <summary>
				/// The Title2 field.
				/// <para></para>
				/// <para>Field Type: Single-Line Text</para>		
				/// <para>Field ID: 4385c3c2-6b6b-4754-bc65-bb5cdf510d54</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.Title2FieldName)]
				public virtual string Title2  {get; set;}
					
		
				/// <summary>
				/// The URL field.
				/// <para></para>
				/// <para>Field Type: General Link</para>		
				/// <para>Field ID: 3bf80f5c-7abd-4f8f-bce8-8c7acb7cb1f2</para>
				/// <para>Custom Data: </para>
				/// </summary>
				[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
				[SitecoreField(IContent_ParaConstants.URLFieldName)]
				public virtual Link URL  {get; set;}
					
			
	}

}

