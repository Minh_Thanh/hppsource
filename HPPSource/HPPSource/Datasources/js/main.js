(function($, window, undefined) {
	var document = window.document,
		location = window.location;

	var modules = {};
	// nav
	modules.nav = function() {
		$('.hd-nav li:has(ul)').addClass('has-ul').find('li > a').prepend('<span class="icon" />');
	};

	// accordion
	modules.accordion = function() {
		$('.accordion').each(function() {
			var 
				$this = $(this),
				$title = $('.title', this),
				$content = $('.content', this);
			$title.click(function() {
				$this.toggleClass('active');
				$content.stop(true).slideToggle();
				return false;
			});
		});
	};

	// tipuesearch */
	modules.tipuesearch = function() {
		$.fn.tipuesearch && $('#tipue_search_input').tipuesearch({
			'show': 8,
			'mode': 'live',
			'highlightEveryTerm': true
		});
	};

	// document ready
	$(function() {
		modules.nav();
		modules.accordion();
		modules.tipuesearch();
	});
})(jQuery, window);