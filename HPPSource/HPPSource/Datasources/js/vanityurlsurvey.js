//This file will initialize the survey views and record the selections in hidden input fields


//Toggle Visibility of DOM object passed to this function by adding active/inactive classes
function toggleVis(IDofObject) {
    if ($(IDofObject).hasClass('active')) {
        $(IDofObject).removeClass('active');
        $(IDofObject).addClass('inactive');
    } else if ($(IDofObject).hasClass('inactive')) {
        $(IDofObject).removeClass('inactive');
        $(IDofObject).addClass('active');
    }
}

//Record button click values in associated hidden form field
function recordAnswer(IDofObject) {
    $(IDofObject).nextAll('input[type=hidden]').attr('value', $(IDofObject).attr('value'));
    
}

//Hide all survey pages except the first
function initSurvey() {
    $('#surveyForm fieldset').addClass('inactive');
    toggleVis($('#surveyForm fieldset:first-of-type'));
}

//Mobile support for button hoverstate
function pseudoHoverstate(IDofObject) {
    $(IDofObject).on('touchstart', function(e) {
	$(this).addClass('hover');
}).on('touchmove', function(e) {
	$(this).removeClass('hover');
}).mouseenter( function(e) {
	$(this).addClass('hover');
}).mouseleave( function(e) {
	$(this).removeClass('hover');
}).click( function(e) {
	$(this).removeClass('hover');
});
    
}

//Do all the things!
$(function() {

//  Survey Form Logic
    if ($("#surveyForm")[0]) {
        initSurvey();
        $('#surveyForm input[type=button]').click(function () {
            var $currentFs = $(this).closest('fieldset'),
                $currentFsNum = $currentFs.index(),
                $nextFs = $(this).closest('fieldset').next('fieldset');
    //        First page, no answers needed
            if ($currentFsNum === $('fieldset').index()) {
                toggleVis($currentFs);
                toggleVis($nextFs);
            } else if ($currentFs.attr('id') === 'q1') {
    //            Not an HCP? Get the boot to the homepage, otherwise continue.
                if ($(this).attr('value') === 'No') {
                    recordAnswer(this);
                    $('#surveyForm').submit();
                } else {
                    recordAnswer(this);
                    toggleVis($currentFs);
                    toggleVis($nextFs);
            }

            } else if ($currentFs.attr('id') === 'q3') {
    //            Question 3 Conditional Statements (Remove Extra Zip field depending on answer)
                if ($(this).attr('value') === 'Yes') {
                    recordAnswer(this);
                    $('#q3 .inactive').removeClass('inactive');
                    $('#q3 .lowerContainer div').addClass('inactive');
                    $('#q3 legend').html('Please fill out the form and a disease specialist from Alexion&nbsp;Pharmaceuticals will contact you.');
                    $("#q4 .zip").remove();
                } else {
                    recordAnswer(this);
                    toggleVis($currentFs);
                    toggleVis($nextFs);
                    $("#q3 .zip").remove();
                }

            } else if ($currentFsNum + 1 === $('fieldset').length) {
    //            Last page functions go here
            } else {
    //            Pages without special contidions
                recordAnswer(this);
                toggleVis($currentFs);
                toggleVis($nextFs);
            }
        });
    }

//    Activate Survey Button Hoverstates
    pseudoHoverstate($('#surveyForm input[type=submit]'));
    pseudoHoverstate($('#surveyForm input[type=button]'));

});