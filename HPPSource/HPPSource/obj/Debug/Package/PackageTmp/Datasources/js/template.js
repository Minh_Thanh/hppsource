

$(function(){
	var id = $("body").attr("id");	
	$("nav.hd-nav ul li a."+id).addClass("current");
});


function header(rootDir){

    $.ajax({
        url: rootDir + "header_navi.html",
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //header.htmlの{$root}を置換
            document.write(html);
        }
    });

}

function footer(rootDir){

    $.ajax({
        url: rootDir + "footer_navi.html",
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //footer.htmlの{$root}を置換
            document.write(html);
        }
    });

}


$(function(){
	var id = $("body").attr("class");	
	$("nav.sidebar-nav ul li a."+id).addClass("current");
});

function hpp_snavi(rootDir){

    $.ajax({
        url: rootDir + "sidenav_navi_hpp.html",
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //snavi.htmlの{$root}を置換
            document.write(html);
        }
    });

}

function systemic_snavi(rootDir){

    $.ajax({
        url: rootDir + "sidenav_navi_systemic.html",
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //snavi.htmlの{$root}を置換
            document.write(html);
        }
    });

}

function diagnosing_snavi(rootDir){

    $.ajax({
        url: rootDir + "sidenav_navi_diagnosing.html",
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //snavi.htmlの{$root}を置換
            document.write(html);
        }
    });

}

function management_snavi(rootDir){

    $.ajax({
        url: rootDir + "sidenav_navi_management.html",
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //snavi.htmlの{$root}を置換
            document.write(html);
        }
    });

}

function resources_snavi(rootDir){

    $.ajax({
        url: rootDir + "sidenav_navi_resources.html",
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //snavi.htmlの{$root}を置換
            document.write(html);
        }
    });

}