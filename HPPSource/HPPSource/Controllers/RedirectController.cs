﻿using Sitecore.Data.Fields;
using Glass.Mapper.Sc.Web.Mvc;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HPPSource.Controllers
{
    public class RedirectController : Controller
    {
        // GET: Redirect
        public ActionResult Index()
        {
            const string REDIRECT_TO_ID = "{34F5965F-B528-42E4-AEA8-FE30ED89D7CA}";
        
            var link = (LinkField)RenderingContext.Current.ContextItem.Fields[REDIRECT_TO_ID];
            if (link != null)
            {
                if (link.IsInternal)
                {
                    return Redirect(LinkManager.GetItemUrl(link.TargetItem));
                }
                else
                {
                    return Redirect(link.Url);
                }
            }

            return View();
        
    }
    }
}