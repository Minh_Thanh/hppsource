﻿using Glass.Mapper.Sc;
using HPPSource.Models;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Mvc;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;

namespace HPPSource.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Header()
        {
            Database currentDb = Sitecore.Context.Database;
            string datasources = RenderingContext.Current.Rendering.DataSource;
            Item item = currentDb.GetItem(datasources);
            //List<Item> items = item.GetChildren().ToList();
            return View("~/Views/Home/Header.cshtml", item);
        }
       
        public ActionResult Feature()
        {
            Database currentDb = Sitecore.Context.Database;
            string datasources = RenderingContext.Current.Rendering.DataSource;
            Item item = currentDb.GetItem(datasources);
            List<Item> items = item.GetChildren().ToList();
            
            return View("~/Views/Home/Home_Page.cshtml", items);

        }
        public ActionResult References()
        {
            Database currentDb = Sitecore.Context.Database;
            string datasources = RenderingContext.Current.Rendering.DataSource;
            Item item = currentDb.GetItem(datasources);
            List<Item> items = item.GetChildren().ToList();

            return View("~/Views/Home/References.cshtml", items);

        }

        public ActionResult Footer()
        {
            

            var datasourceid = RenderingContext.Current.Rendering.DataSource;
            var dataItem = Sitecore.Context.Database.GetItem(datasourceid);
            return View("~/Views/Home/Footer.cshtml", dataItem);
        }

    }
}