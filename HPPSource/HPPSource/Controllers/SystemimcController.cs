﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HPPSource.Controllers
{
    public class SystemimcController : Controller
    {
        // GET: Systemimc
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Navleft()
        {
            Database currentDb = Sitecore.Context.Database;
            string datasources = RenderingContext.Current.Rendering.DataSource;
            Item item = currentDb.GetItem(datasources);
            List<Item> items = item.GetChildren().ToList();

            return View("~/Views/Systemic/Content_Left.cshtml", items);
        }
        public ActionResult LdList()
        {
            Database currentDb = Sitecore.Context.Database;
            string datasources = RenderingContext.Current.Rendering.DataSource;
            Item item = currentDb.GetItem(datasources);
            //List<Item> items = item.GetChildren().ToList();

            return View("~/Views/Systemic/Content_Right.cshtml", item);
        }
    }
}