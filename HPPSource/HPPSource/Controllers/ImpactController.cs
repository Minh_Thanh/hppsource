﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HPPSource.Controllers
{
    public class ImpactController : Controller
    {
        // GET: Impact
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Sidebar()
        {
            Database currentDb = Sitecore.Context.Database;
            string datasources = RenderingContext.Current.Rendering.DataSource;
            Item item = currentDb.GetItem(datasources);
            List<Item> items = item.GetChildren().ToList();

            return View("~/Views/Impact/Content_Left.cshtml", items);
        }
        public ActionResult PageContent()
        {
            Database currentDb = Sitecore.Context.Database;
            string datasources = RenderingContext.Current.Rendering.DataSource;
            Item item = currentDb.GetItem(datasources);
            //List<Item> items = item.GetChildren().ToList();

            return View("~/Views/Impact/Content_Right.cshtml", item);
        }
    }
}